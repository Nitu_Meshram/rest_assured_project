package request_repository;

import java.io.IOException;
import java.util.ArrayList;

import utility_common_methods.Excel_data_extractor;

public class Post_request_repository {

	public static String post_request_tc1() throws IOException {
		ArrayList<String> Data =Excel_data_extractor.Excel_data_reader("TestData", "Post_API", "Post_TC4");
		String Name=Data.get(1);
		String Job=Data.get(2);
		String requestbody = "{\r\n" + "    \"name\": \""+Name+"\",\r\n" + "    \"job\": \""+Job+"\"\r\n" + "}";
		return requestbody;
	}

}
