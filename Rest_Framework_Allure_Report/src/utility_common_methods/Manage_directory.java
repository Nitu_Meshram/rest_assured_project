package utility_common_methods;

import java.io.File;

public class Manage_directory {
	public static File create_log_directory(String logDir) {

		// fetch the current project directory
		String projectDirectory = System.getProperty("user.dir");
		//System.out.println("Path of current project directory is : " + projectDirectory);

		File directory = new File(projectDirectory + "\\API_logs\\" + logDir);
			
		if (directory.exists()) {

			directory.delete();
			//System.out.println(directory + " : deleted successfully");

			directory.mkdir();
			System.out.println("Existing directory deleted and new directory created successfully");
		} else {
			directory.mkdir();
		//	System.out.println(directory + " : created successfully");
		}

		return directory;

	}

}
