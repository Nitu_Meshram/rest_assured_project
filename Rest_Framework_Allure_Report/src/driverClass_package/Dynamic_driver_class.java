package driverClass_package;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;


import utility_common_methods.Excel_data_extractor;

public class Dynamic_driver_class {

	
	public static void main(String[] args) throws IOException, ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		
		ArrayList<String> TC_Execute=Excel_data_extractor.Excel_data_reader("TestData", "Test_Cases", "TC_Name");
		System.out.println(TC_Execute+ "\n");
		int count=TC_Execute.size();
		
		for(int i=1;i<count;i++)
		{
		String TC_name=TC_Execute.get(i);
		System.out.println(TC_name);
		
		// Call the Test_Case class on runtime by using java.lang.reflect.class package
		Class<?> Test_class=Class.forName("TestClass_package."+TC_name);
		
				
		// Call the execute method belonging to Test_class captured in variable tc_name by using java.lang.reflect.method class
		Method execute_method=Test_class.getDeclaredMethod("executor");
				
		// Set the accessibility of method to true
		execute_method.setAccessible(true);
				
		// Create the instance of test classes captured in variable name Test_class
		Object TC_instance=Test_class.getDeclaredConstructor().newInstance();
		
		//Execute the test_class executor method by using instance
		execute_method.invoke(TC_instance);
		
			
		}
	}

}
