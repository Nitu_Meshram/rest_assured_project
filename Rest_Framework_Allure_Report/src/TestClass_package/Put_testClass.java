package TestClass_package;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import org.testng.Assert;
import org.testng.annotations.Test;

import api_common_methods.Common_methods;
import endpoints.Put_endpoint;
import io.restassured.path.json.JsonPath;
import request_repository.Put_request_repository;
import utility_common_methods.Manage_api_logs;
import utility_common_methods.Manage_directory;

public class Put_testClass extends Common_methods {
@Test
	public static void executor() throws IOException {
		File Log_dir=Manage_directory.create_log_directory("Put_TC_logs");
		String requestbody=Put_request_repository.put_request_tc1();
		String endpoint =Put_endpoint.put_endpoint_tc1();
		
			for (int i = 0; i < 5; i++) {
			int status_code = put_statuscode(requestbody, endpoint);
			System.out.println(status_code);
			{
				if (status_code == 200) {
					String responsebody = put_responsebody(requestbody, endpoint);
					System.out.println(responsebody);
					Manage_api_logs.create_evidence(Log_dir, "Put_TC", endpoint, requestbody, responsebody);
					Put_testClass.put_validator(requestbody, responsebody);
					break;
				}

				else {
					System.out.println("Retry since expected statuscode not found");
				}
			}
		}
	}

	public static void put_validator(String requestbody, String responsebody) {
		JsonPath request = new JsonPath(requestbody);
		String req_name = request.getString("name");
		String req_job = request.getString("job");
		LocalDateTime sysdate = LocalDateTime.now();
		String exp_date = sysdate.toString().substring(0, 10);

		JsonPath response = new JsonPath(responsebody);
		String res_name = response.getString("name");
		String res_job = response.getString("job");
		String res_date = response.getString("updatedAt");
		res_date = res_date.substring(0, 10);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_date, exp_date);

	}
}
