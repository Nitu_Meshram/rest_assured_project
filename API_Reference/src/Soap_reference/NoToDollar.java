package Soap_reference;
import static io.restassured.RestAssured.given;
import org.testng.Assert;
import io.restassured.path.xml.XmlPath;
public class NoToDollar {

	public static void main(String[] args) {
		// Step 1 Declare the Base URL
		//String BaseURI="https://www.dataaccess.com";

		//Step 2 Declare the request body
		String requestBody="<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:web=\"http://www.dataaccess.com/webservicesserver/\">\r\n"
				+ "   <soap:Header/>\r\n"
				+ "   <soap:Body>\r\n"
				+ "      <web:NumberToDollars>\r\n"
				+ "         <web:dNum>23.5</web:dNum>\r\n"
				+ "      </web:NumberToDollars>\r\n"
				+ "   </soap:Body>\r\n"
				+ "</soap:Envelope>\r\n";
		
		// Step 3 trigger the API and fetch the response body
		//RestAssured.baseURI=BaseURI;
		String responseBody=given().header("Content-Type", "text/xml; charset=utf-8").
		when().body(requestBody).post("https://www.dataaccess.com/webservicesserver/NumberConversion.wso").
		then().extract().response().getBody().asString();
		System.out.println(responseBody);

		//Step 4 extract response body parameter
		XmlPath res_xml=new XmlPath(responseBody);
		String res_tag=res_xml.getString("NumberToDollarsResult");
		//System.out.println(res_tag);

		//Step 5 validate the response body
		Assert.assertEquals(res_tag,"twenty three dollars and fifty cents");

		}

		}
