package Soap_reference;
import static io.restassured.RestAssured.given;

import org.testng.Assert;

import io.restassured.path.xml.XmlPath;
public class NoToWords {
public static void main(String[] args) {

		// Step 1 Declare the Base URL
//String BaseURI="https://www.dataaccess.com";

//Step 2 Declare the request body
String requestBody="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n"
		+ "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n"
		+ "  <soap:Body>\r\n"
		+ "    <NumberToWords xmlns=\"http://www.dataaccess.com/webservicesserver/\">\r\n"
		+ "      <ubiNum>500</ubiNum>\r\n"
		+ "    </NumberToWords>\r\n"
		+ "  </soap:Body>\r\n"
		+ "</soap:Envelope>";

// Step 3 trigger the API and fetch the response body
//RestAssured.baseURI=BaseURI;
String responseBody=given().header("Content-Type", "text/xml; charset=utf-8").
body(requestBody).when().post("https://www.dataaccess.com/webservicesserver/NumberConversion.wso").
then().extract().response().getBody().asString();
System.out.println(responseBody);

//Step 4 extract response body parameter
XmlPath res_xml=new XmlPath(responseBody);
String res_tag=res_xml.getString("NumberToWordsResult");
//System.out.println(res_tag);

//Step 5 validate the response body
Assert.assertEquals(res_tag, "five hundred ");

}

}
