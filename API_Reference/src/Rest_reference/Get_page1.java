package Rest_reference;

import static io.restassured.RestAssured.given;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;

import io.restassured.RestAssured;

public class Get_page1 {

	public static void main(String[] args) {
		// Step 1 declare base URL
				RestAssured.baseURI="https://reqres.in";
				
				//Step 2 Validation of status code
				int status_code=given().header("Content-Type", "application/json").
				when().get("/api/users?page=1").then().extract().response().statusCode();
				//System.out.println(status_code);
				Assert.assertEquals(status_code, 200);
				
				//Step 3 Extract response body into local variable
				String responsebody=given().header("Content-Type", "application/json").
			    when().get("/api/users?page=1").then().extract().response().asString();
			    System.out.println("responsebody is :"+responsebody);
			    
			    //Step 4 Declare expected result
			    int id[]= {1,2,3,4,5,6};
			    String email[]= {"george.bluth@reqres.in","janet.weaver@reqres.in",
			    		"emma.wong@reqres.in", "eve.holt@reqres.in", "charles.morris@reqres.in",
			    		"tracey.ramos@reqres.in"};
			    String fname[]= {"George","Janet","Emma","Eve","Charles","Tracey"};
			    String lname[]= {"Bluth","Weaver","Wong", "Holt","Morris","Ramos"};
			   	    
			    //Step 5 Create JSONObject object to parse response body
			    JSONObject res_array=new JSONObject(responsebody);
			    JSONArray data_array=res_array.getJSONArray("data");
			    int count =data_array.length();
			    
			    //Step 6 Validation of response Body
			    for (int i=0;i<count;i++)
			    {
			    int exp_id=id[i];
			    String exp_email=email[i];
				String exp_fname=fname[i];
				String exp_lname=lname[i];
				
				int res_id=data_array.getJSONObject(i).getInt("id");
				String res_email=data_array.getJSONObject(i).getString("email");
				String res_fname=data_array.getJSONObject(i).getString("first_name");
				String res_lname=data_array.getJSONObject(i).getString("last_name");
				
				 Assert.assertEquals(res_id,exp_id);
				 Assert.assertEquals(res_email,exp_email);
				 Assert.assertEquals(res_fname, exp_fname);
				 Assert.assertEquals(res_lname,exp_lname);
				 }

	}

}
