package Rest_reference;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class Patch_reference {

	public static void main(String[] args) {
		// Step 1 Declare base URL
				RestAssured.baseURI = "https://reqres.in";
				
				// Step 2 Configure request parameters and trigger the API
				String requestbody="{\r\n"
						+ "    \"name\": \"morpheus\",\r\n"
						+ "    \"job\": \"zion resident\"\r\n"
						+ "}";
				//int status_code=given().header("Content-Type", "application/json").body(requestbody).
						//when().put("/api/users/2").then().extract().response().statusCode();
				//Assert.assertEquals(status_code, 200);
				//System.out.println(status_code);
				String responsebody = given().header("Content-Type", "application/json").body(requestbody).
				when().put("/api/users/2").then().extract().response().asString();
				System.out.println("responsebody is :" +responsebody);
				// Step 3 create an object of Json path to parse request body and then response body
				JsonPath jsp_req=new JsonPath(requestbody);
				String req_name=jsp_req.getString("name");
				String req_job=jsp_req.getString("job");
				LocalDateTime cur_date=LocalDateTime.now();
				String exp_date=cur_date.toString().substring(0, 10);
				
				JsonPath jsp_res=new JsonPath(responsebody);
				String res_name=jsp_res.getString("name");
				String res_job=jsp_res.getString("job");
				String res_date=jsp_res.getString("updatedAt");
				res_date=res_date.substring(0,10);
				
				//Step 4 validate responsebody
				Assert.assertEquals(res_name, req_name);
				Assert.assertEquals(res_job, req_job);
				Assert.assertEquals(res_date,exp_date);
					

	}

}
