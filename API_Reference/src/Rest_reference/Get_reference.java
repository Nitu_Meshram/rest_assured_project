package Rest_reference;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class Get_reference {

	public static void main(String[] args) {
		// step 1 Declare base URL
				RestAssured.baseURI = "https://reqres.in";
				
				// step 2 Trigger an API
				String responsebody = given().header("Content-Type", "application/json").
			    when().get("/api/users/2").then().extract().response().asString();
				System.out.println("responsebody is :" +responsebody);
				
				//Step 3 Create object of Json path to parse response body
				JsonPath res_jsp=new JsonPath(responsebody);
				
				int res_id=res_jsp.getInt("data.id");
				//System.out.println(res_id);
				String res_email=res_jsp.getString("data.email");
				//System.out.println(res_email);
				String res_fname=res_jsp.getString("data.first_name");
				String res_lname=res_jsp.getString("data.last_name");
				String res_avatar=res_jsp.getString("data.avatar");
				
				//Step 4 validate responsebody
				Assert.assertEquals(res_id, 2);
				Assert.assertEquals(res_email,"janet.weaver@reqres.in");
				Assert.assertNotNull(res_fname,"Janet");
				Assert.assertEquals(res_lname,"Weaver");
				Assert.assertEquals(res_avatar,"https://reqres.in/img/faces/2-image.jpg");

	}

}
