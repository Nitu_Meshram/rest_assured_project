package Rest_reference;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

import io.restassured.RestAssured;

public class Delete_reference {

	public static void main(String[] args) {
		// step 1 declare base URI
				RestAssured.baseURI = "https://reqres.in";

				// step 2 trigger the API and validate the status code
				int status_code = given().header("Content-Type", "application/json").when().delete("/api/users/2").then()
						.extract().response().statusCode();
				Assert.assertEquals(status_code, 204);
				System.out.println(status_code);

	}

}
