package Rest_reference;

import static io.restassured.RestAssured.given;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;

import io.restassured.RestAssured;

public class Get_page2 {

	public static void main(String[] args) {
		  // Step 1 Declare Base URL
		RestAssured.baseURI="https://reqres.in";
		
		// Step 2 trigger the API and extract  response in to local variable
		String responsebody=given().header("Content-Type", "application/json").
		when().get("/api/users?page=2").then().extract().response().asString();
		System.out.println("responsebody is :"+responsebody);
		
		//Step 3 Extraction and validation of status code
		int status_code=given().header("Content-Type", "application/json").
		when().get("/api/users?page=2").then().extract().statusCode();
		System.out.println(status_code);
		Assert.assertEquals(status_code, 200);
					
		//Step 4 Declare expected results
		int expected_id[]={7,8,9,10,11,12};
		String expected_fname[]= {"Michael","Lindsay","Tobias", "Byron","George","Rachel"};
		String expected_lname[]= {"Lawson","Ferguson","Funke","Fields","Edwards","Howell"};
		String expected_email[]= {"michael.lawson@reqres.in","lindsay.ferguson@reqres.in","tobias.funke@reqres.in",
				                  "byron.fields@reqres.in","george.edwards@reqres.in","rachel.howell@reqres.in"};
		String expected_avatar[]= {"https://reqres.in/img/faces/7-image.jpg","https://reqres.in/img/faces/8-image.jpg",
				                   "https://reqres.in/img/faces/9-image.jpg","https://reqres.in/img/faces/10-image.jpg",
				                   "https://reqres.in/img/faces/11-image.jpg","https://reqres.in/img/faces/12-image.jpg"};
		
		// Step 4 Create JsonObject object to parse response body and extract data array from it
		//JsonPath res_jsp=new JsonPath(responsebody);
		//String res_dataarray=res_jsp.getString("data");
		//System.out.println(res_dataarray);
		JSONObject res_array=new JSONObject(responsebody);
		//System.out.println(res_array);
		JSONArray dataArray = res_array.getJSONArray("data");
		//System.out.println(dataArray);
		int count =dataArray.length();
		
		// Step 5 Validation of response body
		for (int i=0;i<count;i++)
		{
			int id=expected_id[i];
			String fname=expected_fname[i];
			String lname=expected_lname[i];
			String email=expected_email[i];
			String avatar=expected_avatar[i];
			
			int res_id=dataArray.getJSONObject(i).getInt("id");
			String res_fname=dataArray.getJSONObject(i).getString("first_name");
			String res_lname=dataArray.getJSONObject(i).getString("last_name");
			String res_email=dataArray.getJSONObject(i).getString("email");
			String res_avatar=dataArray.getJSONObject(i).getString("avatar");
		
		
		    Assert.assertEquals(res_id,id);
		    Assert.assertEquals(res_fname, fname);
		    Assert.assertEquals(res_lname,lname);
		    Assert.assertEquals(res_email,email);
		    Assert.assertEquals(res_avatar,avatar);
				
		}
		
	}

}
