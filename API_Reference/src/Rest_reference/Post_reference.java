package Rest_reference;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class Post_reference {

	public static void main(String[] args) {
		// Step 1: Declare base URI
				RestAssured.baseURI = "https://reqres.in";
				//String BaseURI="https://reqres.in";
				
				// Step 2 extract request body in to local variable
				String requestbody="{\r\n"
						+ "    \"name\": \"morpheus\",\r\n"
						+ "    \"job\": \"leader\"\r\n"
						+ "}";
				
				// Step 3 Extract Status code
				int StatusCode = given().header("Content-Type", "application/json").body(requestbody).
						when().post("/api/users").then().extract().statusCode();
						System.out.println(StatusCode);
				
				// Step 3 Extract response body into local variables
				String responsebody = given().header("Content-Type", "application/json").body(requestbody).
				when().post("/api/users").then().extract().response().asString();
				System.out.println("responsebody is :" + responsebody);
				
				// Step 4 create an object of Json path to parse request body 
				JsonPath jsp_req=new JsonPath(requestbody);
				
				// Step 5 Extract request body parameters
				String req_name=jsp_req.getString("name");
				String req_job=jsp_req.getString("job");
				LocalDateTime cur_date=LocalDateTime.now();
				//System.out.println(cur_date);
				String exp_date=cur_date.toString().substring(0,10);
				//System.out.println(exp_date);
				
				//Step 6 Create an object of json path for response body
				JsonPath jsp_res=new JsonPath(responsebody);
				
				//Step 7 Extract response body parameters
				String res_name=jsp_res.getString("name");
				String res_job=jsp_res.getString("job");
				String res_id=jsp_res.getString("id");
				String res_date=jsp_res.getString("createdAt").substring(0,10);
				//res_date=res_date.substring(0,10);
				//System.out.println(res_date);
				
				//Step 8 validate status code and response body
				Assert.assertEquals(StatusCode, 201);
				Assert.assertEquals(res_name, req_name);
				Assert.assertEquals(res_job, req_job);
				Assert.assertNotNull(res_id);
				Assert.assertNotEquals(res_id, 0);
				Assert.assertEquals(res_date,exp_date);
			
			}

	}


