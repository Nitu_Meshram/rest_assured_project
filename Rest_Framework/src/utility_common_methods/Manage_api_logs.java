package utility_common_methods;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Manage_api_logs {

	public static void create_evidence (File DirName, String FileName, String endpoint, String requestbody,
			String responsebody) throws IOException
	
	{
		File NewFile = new File(DirName+"\\"+FileName +".txt");
		System.out.println("To save request and response body we have created a new file named :" + NewFile.getName()+"\n");

		FileWriter dw= new FileWriter(NewFile);
		dw.write("Endpoint is :" + endpoint + "\n\n");
		dw.write("Request Body is :" + requestbody + "\n\n");
		dw.write("Response Body is :" + responsebody);
		dw.close();
	}
	
}
	

