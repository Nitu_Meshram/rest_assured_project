package TestClass_package;

import org.testng.annotations.Test;

import api_common_methods.Common_methods;
import endpoints.Delete_endpoint;

@Test
public class delete_testClass extends Common_methods {
	public static void executor() {
		
		String endpoint = Delete_endpoint.delete_endpoint();

		for (int i = 0; i < 5; i++) {
			int statuscode = delete_statuscode(endpoint);

			if (statuscode == 204) {
				System.out.println(statuscode);
				break;
			} else {
				System.out.println("Expected statuscode not found hence retrying");
			}
		}
	}
}