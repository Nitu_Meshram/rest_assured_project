# Rest_Assured_Project
My framework consists of six major components namely,
1.test execution mechanism,
2.test scripts,
3.common function,
4.variable file,
5.libraries and
6.reports.

Test execution mechanism: --> In test execution mechanism I am using static driver class, and dynamic driver class for test case execution. While using static driver class  I have to explicitly select the test script everytime, so I have created dynamic driver class to call test cases dynamically using java.lang.reflect package. 

Test Script: --> For each API we have multiple test cases and for each test case we have one single test script, which I have created in Test classes pkg.  

Common Function: --> Then in common function, I segregated it into two categories API related common function and Utilities related common function. 
In API_common_function I have created methods to trigger the API, fetch the responsebody and extract response status code.

Utilities_common_function, contains methods for creating API logs (endpoint, requestbody, responsebody) once the API is executed and saving them into seperate text files. Second utility is craeting a directory if it does not exist and deleting and then creating it if it already exist. Then I have third utility to read data from excel file using apache poi library. 

Variable File: --> In variable file I have used excel file to drive data into my test cases and execute same test case multiple times on different sets od data.

Libraries: --> In libraries I have added Rest Assured Jars for triggering the API, extarcting statuscode, extracting responsebody, parsing response body using JsonPath. Having TestNG library for validating the response parameters using assert class. Then also used apache poi libraries to read and write data from excel file. 

Reports: --> At last I have reporting mechanism where I am using Extent report currently earlier used Allure report as well but looking in to it's cacheing problem started using Extent report.

