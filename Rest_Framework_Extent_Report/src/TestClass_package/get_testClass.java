package TestClass_package;

import java.io.File;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import api_common_methods.Common_methods;
import endpoints.Get_endpoint;
import utility_common_methods.Manage_api_logs;
import utility_common_methods.Manage_directory;

public class get_testClass extends Common_methods {

	@Test
	public static void executor() throws IOException {
		File log_dir=Manage_directory.create_log_directory("Get_tc_logs");
		String endpoint = Get_endpoint.get_endpoint();

		for (int i = 0; i < 5; i++) {
			int statuscode = get_statuscode(endpoint);
			System.out.println(statuscode);
			{
				if (statuscode == 200) {
					String responsebody = get_responsebody(endpoint);
					System.out.println(responsebody);
					Manage_api_logs.create_evidence(log_dir, "Get_tc",endpoint, null, responsebody);
					get_validator(responsebody);
					break;
				}

				else {
					System.out.println("Retry since expected status code not found");
				}

			}
		}
	}

	public static void get_validator(String responsebody) {
		int id[] = { 1, 2, 3, 4, 5, 6 };
		String email[] = { "george.bluth@reqres.in", "janet.weaver@reqres.in", "emma.wong@reqres.in",
				"eve.holt@reqres.in", "charles.morris@reqres.in", "tracey.ramos@reqres.in" };
		String fname[] = { "George", "Janet", "Emma", "Eve", "Charles", "Tracey" };
		String lname[] = { "Bluth", "Weaver", "Wong", "Holt", "Morris", "Ramos" };

		JSONObject res_array = new JSONObject(responsebody);
		JSONArray data_array = res_array.getJSONArray("data");
		int count = data_array.length();

		for (int i = 0; i < count; i++) {
			int exp_id = id[i];
			String exp_email = email[i];
			String exp_fname = fname[i];
			String exp_lname = lname[i];

			int res_id = data_array.getJSONObject(i).getInt("id");
			String res_email = data_array.getJSONObject(i).getString("email");
			String res_fname = data_array.getJSONObject(i).getString("first_name");
			String res_lname = data_array.getJSONObject(i).getString("last_name");

			Assert.assertEquals(res_id, exp_id);
			Assert.assertEquals(res_email, exp_email);
			Assert.assertEquals(res_fname, exp_fname);
			Assert.assertEquals(res_lname, exp_lname);
		}

	}

}
