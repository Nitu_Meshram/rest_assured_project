package TestClass_package;
import io.restassured.path.json.JsonPath;
import request_repository.Post_request_repository;
import utility_common_methods.Manage_api_logs;
import utility_common_methods.Manage_directory;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import org.testng.Assert;
import org.testng.annotations.Test;

import api_common_methods.Common_methods;
import endpoints.Post_endpoint;

public class Post_test_class extends Common_methods {
	
	@Test
	public static void executor() throws IOException {
		File Log_dir=Manage_directory.create_log_directory("Post_tc_Logs");
		String requestbody=Post_request_repository.post_request_tc1();
		String endpoint =Post_endpoint.post_endpoint_tc1();
		for (int i = 0; i < 5; i++) {
			int statuscode = post_statuscode(requestbody, endpoint);
			System.out.println(statuscode);
			{
				if (statuscode == 201) {
					String responsebody = post_responsebody(requestbody, endpoint);
					System.out.println(responsebody);
					Manage_api_logs.create_evidence(Log_dir,"Post_tc", endpoint, requestbody, responsebody);
					post_validator(requestbody, responsebody);
					break;
				}

				else {
					System.out.println("Retry as expected status code 201 not found");
				}
			}

		}
	}

	public static void post_validator(String requestbody, String responsebody) {
		
		JsonPath req_json = new JsonPath(requestbody);
		String req_name = req_json.getString("name");
		String req_job = req_json.getString("job");
		LocalDateTime sysdate = LocalDateTime.now();
		String exp_date = sysdate.toString().substring(0, 10);

		JsonPath res_json = new JsonPath(responsebody);
		String res_name = res_json.getString("name");
		String res_job = res_json.getString("job");
		String res_date = res_json.getString("createdAt").substring(0,10);
		//res_date = res_date.substring(0, 10);
		String res_id = res_json.getString("id");

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_date, exp_date);
		Assert.assertNotEquals(res_id, 0);
		Assert.assertNotNull(res_id);

	}
}
