package utility_common_methods;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel_data_extractor {

	public static ArrayList<String> Excel_data_reader(String fileName, String sheetName, String tcName)
			throws IOException {
		ArrayList<String> Arraydata = new ArrayList<String>();
		String projectDirectory = System.getProperty("user.dir");

		// Step 1 create the object of file input stream to locate the data file
		FileInputStream FIS = new FileInputStream(projectDirectory + "\\DataFile\\" + fileName + ".xlsx");

		// Step 2 Create the XSSFWorkbook object to open the excel file
		XSSFWorkbook WB = new XSSFWorkbook(FIS);

		// Step 3 fetch the number of sheets available in the excel file
		int count = WB.getNumberOfSheets();

		// Step 4 access the sheet as per the input sheet name

		for (int i = 0; i < count; i++) {
			String SheetName = WB.getSheetName(i);

			if (SheetName.equals(sheetName)) {
				System.out.println(SheetName);
				XSSFSheet SHeet = WB.getSheetAt(i);
				Iterator<Row> row = SHeet.iterator();
				//row.next();
				while (row.hasNext()) {
					Row data_row = row.next();
					String TC_name = data_row.getCell(0).getStringCellValue();
					if (TC_name.equals(tcName)) {
						Iterator<Cell> cellvalues = data_row.iterator();
						while (cellvalues.hasNext()) {
							String testdata = cellvalues.next().getStringCellValue();
							Arraydata.add(testdata);
						}

					}
				}
				break;
			}

		}
		WB.close();
		return Arraydata;

	}

}
