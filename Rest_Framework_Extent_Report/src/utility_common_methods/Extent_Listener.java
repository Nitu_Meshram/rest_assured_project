package utility_common_methods;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class Extent_Listener implements ITestListener {
	ExtentSparkReporter sparkreporter;
	ExtentReports extentReport;
	ExtentTest test;
	public void reportConfiguration()
	{
		sparkreporter=new ExtentSparkReporter(".\\Extent-report\\report.html");
		extentReport=new ExtentReports();
		extentReport.attachReporter(sparkreporter);
		extentReport.setSystemInfo("OS", "Windows 10 Pro");
		extentReport.setSystemInfo("Users", "USER");
		
		sparkreporter.config().setDocumentTitle("Rest Assured Extent report");
		sparkreporter.config().setReportName("My first Extent-Report");
		sparkreporter.config().setTheme(Theme.DARK);
	}
	
	
	public void onStart(ITestContext result) {
		reportConfiguration();
		System.out.println("On Start " +result.getName()+" method invoked ");
		
	}

	public void onFinish(ITestContext result) {
		System.out.println("On Finish "+result.getName()+ " method invoked ");
		extentReport.flush();

	}

	public void onTestFailure(ITestResult result) {
		System.out.println(result.getName()+ " test method failed ");
		test=extentReport.createTest(result.getName());
		test.log(Status.FAIL, MarkupHelper.createLabel("Name of the failed test case is "+result.getName(), ExtentColor.RED));

	}

	public void onTestSkipped(ITestResult result) {
		System.out.println(result.getName()+" test method skipped ");
		test=extentReport.createTest(result.getName());
		test.log(Status.SKIP, MarkupHelper.createLabel("Name of the skipped test case is "+result.getName(), ExtentColor.YELLOW));

	}

	public void onTestStart(ITestResult result) {
		System.out.println(result.getName()+ " test Method started " );

	}

	public void onTestSuccess(ITestResult result) {
		System.out.println(result.getName()+" test Method executed successfully");
		test=extentReport.createTest(result.getName());
		test.log(Status.PASS, MarkupHelper.createLabel("Name of the passed test case is "+result.getName(), ExtentColor.GREEN));
	}

	public void onTestFailedButWithinSucessPercentage(ITestResult result) {
		// No implementation
	}

}
