package request_repository;

import java.io.IOException;
import java.util.ArrayList;

import utility_common_methods.Excel_data_extractor;

public class Put_request_repository {
	
	public static String put_request_tc1() throws IOException {
		ArrayList<String> Data=Excel_data_extractor.Excel_data_reader("TestData", "Put_API", "Put_TC3");
		String name=Data.get(1);
		String job=Data.get(2);
		String requestbody =  "{\r\n" + "    \"name\": \""+name+"\",\r\n" + "    \"job\": \""+job+"\"\r\n" + "}";
		return requestbody;
	}
	
}
